# tomcat-force-gc
Docker Image that forces a garbage collection via jmx used for tomcat
The server needs to be accessible via the name "tomcat". jmx has to be run on port: 9213

## Environment Variables

Variable | Descreption | Default
---------|----------|---------
 GC_SLEEP | Time between checks in seconds | 10
 GC_MAX_MEMORY | Limit for GC. If Limit is reached GC will be run | 2000000000 = 20 GB
 JMX_HOST | Hostname or IP of host | tomcat
 JMX_PORT | port of jmx-service at host | 9213