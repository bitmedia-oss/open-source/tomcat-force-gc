#!/bin/sh

#set correct host and port from env-variables
sed -i s/jmx_port/$JMX_PORT/ /root/read_heapmemory.jmx
sed -i s/jmx_host/$JMX_HOST/ /root/read_heapmemory.jmx
sed -i s/jmx_port/$JMX_PORT/ /root/run_gc.jmx
sed -i s/jmx_host/$JMX_HOST/ /root/run_gc.jmx

JMXTERMCMD='java -jar /root/jmxterm-1.0.1-uber.jar -i /root/read_heapmemory.jmx'
GCCMD='java -jar /root/jmxterm-1.0.1-uber.jar -i /root/run_gc.jmx'
while :
do
    echo "Requesting used heap memory"
    RESULT=`$JMXTERMCMD 2>&1 | grep HeapMemoryUsage.used | sed s/HeapMemoryUsage.used\ =\ // | sed s/\;//`
    echo $RESULT
    if [ "$RESULT" -gt "$GC_MAX_MEMORY" ];then
        echo "Used memory is higher than $GC_MAX_MEMORY";
        $GCCMD
    fi
    sleep $GC_SLEEP
done