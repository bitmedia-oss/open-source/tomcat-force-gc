FROM openjdk:8-jre
RUN curl -L https://github.com/jiaqi/jmxterm/releases/download/v1.0.1/jmxterm-1.0.1-uber.jar -o /root/jmxterm-1.0.1-uber.jar
ADD entry-point.sh /usr/local/bin/
ADD read_heapmemory.jmx /root
ADD run_gc.jmx /root
RUN chmod +x /usr/local/bin/entry-point.sh
ENV GC_SLEEP 10
ENV GC_MAX_MEMORY 20000000000
ENV JMX_HOST tomcat
ENV JMX_PORT 9213

ENTRYPOINT ["entry-point.sh"]